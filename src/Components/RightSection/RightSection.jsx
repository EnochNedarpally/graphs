import { BubbleChart } from "@mui/icons-material";
import "./RightSection.css";

const RightSection = () => {
  return (
    <div className="rightContainer">
      <div className="graphName">
        <BubbleChart className="icon" fontSize="large"/>
        <span>Directed Graph</span>
      </div>
    </div>
  );
};

export default RightSection;
