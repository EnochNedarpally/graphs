import "./MidSection.css";
import * as d3 from 'd3'
import { useEffect, useRef } from "react";


export default function MidSection() {
 const svgRef =useRef();

 const data = [
  { id: 1, source: "Harish", target: "Hairy", edge: "is_nickname_of" },
];

 useEffect(()=>{
  const nodes = [{ name: data[0].source }, { name: data[0].target }];
  const links = [{ source: data[0].source, target: data[0].target }];
   const svg=d3.select(svgRef.current);
   const width = 500;
   const height = 350;

   

 const link = svg
   .append("g")
   .selectAll("line")
   .data(links)
   .enter()
   .append("line")
   .attr("stroke-width", 3)
   .style("stroke", "gray");

//  link.append("text").text(d => d.name);


   

   const node = svg
   .selectAll("circle")
   .data(nodes)
   .enter()
   .append("circle")
   .attr("r", 30)
   .style("fill", "#93b985");

   const text = svg
   .append("g")
    .selectAll("text")
    .data(nodes)
    .enter()
    .append("text")
    .text(d=> d.name)

   const drag = d3
    .drag()
    .on("start", dragstarted)
    .on("drag", dragged)
    .on("end", dragended);

  node.call(drag);

   const ticked=()=> {
     
    text.attr("x",d=>d.x-20);
    text.attr("y",d=>d.y);

    node
      .attr("cx", function(d) {
        return d.x;
      })
      .attr("cy", function(d) {
        return d.y;
      });

    link
      .attr("x1", function(d) {
        return d.source.x;
      })
      .attr("y1", function(d) {
        return d.source.y;
      })
      .attr("x2", function(d) {
        return d.target.x;
      })
      .attr("y2", function(d) {
        return d.target.y;
      });

    
    
  }
  function dragstarted(d,event) {
    //your alpha hit 0 it stops! make it run again
    simulation.alphaTarget(0.3).restart();
    d.fx = event.x;
    d.fy = event.y;
  }
  function dragged(d,event) {
    d.fx = event.x;
    d.fy = event.y;
  }

  function dragended(d) {
    // alpha min is 0, head there
    simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
  }
  console.log(svgRef.current)
  const simulation = d3
   .forceSimulation(nodes)
   .force("charge", d3.forceManyBody().strength(-7000))
   .force("center", d3.forceCenter(width / 2, height / 2))
   .force("link", d3.forceLink(links).id(d => d.name))
   .on("tick", ticked);
 },[])
  return(
      <div className="midContainer">
        <svg ref={svgRef}></svg>
      </div>
  ) 
}
