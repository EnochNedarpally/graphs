import  './LeftSection.css'

const LeftSection = () => {
    return (
        <div className='leftContainer'>
            <h1 className="logo">Graphs</h1>
            <h3 className="title">Working with Graphs</h3>
            <p className="para">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Earum magnam, illum reprehenderit quos non laboriosam neque doloribus nulla assumenda a perspiciatis fugit obcaecati rerum autem blanditiis ea praesentium. Quos iure maiores molestiae ea tempora fugiat placeat vitae, ipsa odit quam magnam quae harum facilis voluptate a beatae, ducimus temporibus culpa.</p>
            <p className="para">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Earum magnam, illum reprehenderit quos non laboriosam neque doloribus nulla assumenda a perspiciatis fugit obcaecati rerum autem blanditiis ea praesentium. Quos iure maiores molestiae ea tempora fugiat placeat vitae, ipsa odit quam magnam quae harum facilis voluptate a beatae, ducimus temporibus culpa. Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis, nesciunt. Id ipsam nihil dolor animi tempora atque quisquam quasi? Commodi! </p>
        </div>
    )
}

export default LeftSection
