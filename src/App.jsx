import LeftSection from "./Components/LeftSection/LeftSection";
import MidSection from "./Components/MidSection/MidSection";
import RightSection from "./Components/RightSection/RightSection";
import './App.css'

const App = () => {
  return <div className="container">
  <LeftSection/>
  <MidSection/>
  <RightSection/>

  </div>;
};

export default App;